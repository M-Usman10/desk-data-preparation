# from Tools.postprocessing.visualize import visualize_boxes_in_dir
#
# visualize_boxes_in_dir("/home/usman/Subset1")
# from preprocessing.converters import dense_box_to_file_box
# dense_box_to_file_box("/home/usman/PycharmProjects/keras-yolo3/Data/OpenImages/Subset1/Txt_results/boxes.txt","/home/usman/Desktop/experiment")

from Tools.preprocessing.converters import dense_box_to_file_box

# import numpy as np
# Inp_path="/home/usman/PycharmProjects/keras-yolo3/Data/OpenImages/Subset{}/Txt_results/boxes.txt"
# OutPath="/home/usman/PycharmProjects/keras-yolo3/Data/OpenImages/Subset{}/labels"
# all_names=[]
# found=np.load("Found.npy")
# for i in range(1,11):
#     dense_box_to_file_box(Inp_path.format(i),OutPath.format(i),allowed_objects=found)
from Tools.preprocessing.readers import read_images,read_boxes_from_txt
from Tools.preprocessing.resizers import resize_image
from Tools.postprocessing.detection import non_max_suppression
from Tools.postprocessing.visualize import visualize
import os
import numpy as np

colors={"person":(255,255,0),"chair":(255,255,255),"bottle":(0,0,255),"Furniture":(0,0,255),"diningtable":(255,0,0),"Table":(255,0,0),"wineglass":(50,50,50),"cup":(0,0,0)}
def preprocess(img, shape=(416,416,3)):
    img=resize_image(img, shape[0], shape[1])
    return img[0],img[2],img[3]

imgs_dir="/home/usman/PycharmProjects/keras-yolo3/Data/OpenImages/Subset1"
boxes_dir="/home/usman/PycharmProjects/keras-yolo3/Data/OpenImages/Subset1/fixed_labels"
imgs, imgs_names = read_images(imgs_dir, format=".jpg", sorted=False, preprocess=preprocess, total=50)
imgs2, imgs_names = read_images(imgs_dir, format=".jpg", sorted=False, preprocess=None, total=50)
new_imgs=imgs
boxes_paths=[]
for i in range(len(imgs_names)):
    boxes_paths.append(os.path.join(boxes_dir,imgs_names[i].split('.')[0]+'.txt'))

allowed=np.load("./../Found.npy")
boxes, obj_names = read_boxes_from_txt(boxes_paths, allowed_objects=None)

res = []
for i in range(len(imgs_names)):
    pick = non_max_suppression(np.array(boxes[i]).astype(float).astype(int), 0.8)
    res.append(
        visualize(imgs2[i].copy(), np.array(boxes[i])[pick], names=np.array(obj_names[i])[pick],
                  dict_=colors))
