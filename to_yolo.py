
from Tools.preprocessing.readers import read_images,read_boxes_from_txt,read_boxes_from_txt2
from Tools.preprocessing.resizers import resize_image
from Tools.postprocessing.detection import non_max_suppression
from Tools.preprocessing.savers import save_boxes_yolo
import os
import numpy as np
mapping_txt="/home/usman/PycharmProjects/keras-yolo3/model_data/coco_classes2.txt"
dict_={}
with open(mapping_txt,"r") as file:
    for idx,line in enumerate(file):
        dict_[line.strip()]=str(idx)
print(dict_)
dict_["Desk"]=dict_["diningtable"]
dict_["Table"]=dict_["diningtable"]
dict_["Bottle"]=dict_["bottle"]
colors={"person":(255,255,0),"chair":(255,255,255),"bottle":(0,0,255),"Furniture":(0,0,255),"diningtable":(255,0,0),"Table":(255,0,0),"wineglass":(50,50,50),"cup":(0,0,0)}
def preprocess(img, shape=(416,416,3)):
    img=resize_image(img, shape[0], shape[1])
    return img[0],img[2],img[3]
for i in range(1,11):
    imgs_dir="/home/usman/PycharmProjects/keras-yolo3/Data/OpenImages/Subset{}".format(i)
    save_dir="Data/OpenImages/Subset{}".format(i)
    boxes_dir="/home/usman/PycharmProjects/keras-yolo3/Data/OpenImages/Subset{}/labels".format(i)
    imgs, imgs_names = read_images(imgs_dir, format=".jpg", sorted=False, preprocess=preprocess)
    new_imgs=imgs
    boxes_paths=[]
    for i in range(len(imgs_names)):
        boxes_paths.append(os.path.join(boxes_dir,imgs_names[i].split('.')[0]+'.txt'))

    allowed=np.load("./../Found.npy")
    boxes, obj_names = read_boxes_from_txt(boxes_paths, allowed_objects=allowed)

    allowed=["Table","Desk","Bottle"]
    boxes2, obj_names2 = read_boxes_from_txt2(boxes_paths, allowed_objects=allowed)
    imgs = []
    for i in range(len(new_imgs)):
        imgs.append(new_imgs[i][0])
        for boox_idx in range(len(boxes[i])):
            boxes[i][boox_idx]=np.array(boxes[i][boox_idx]).astype(float)
            boxes[i][boox_idx][0]-=new_imgs[i][2][0][0]
            boxes[i][boox_idx][2]-=new_imgs[i][2][0][0]
            boxes[i][boox_idx][1] -= new_imgs[i][2][1][0]
            boxes[i][boox_idx][3] -= new_imgs[i][2][1][0]
            boxes[i][boox_idx] /= new_imgs[i][1]
    temp_boxes=boxes
    temp_names=obj_names
    boxes=[]
    obj_names=[]
    for i in range(len(imgs_names)):
        if np.array(temp_boxes[i]).shape[0]!=0:
            frame_boxes=np.append(np.array(temp_boxes[i]).astype(float),np.array(boxes2[i]).astype(float),axis=0)
            boxes.append(frame_boxes)
            frame_names=np.append(np.array(temp_names[i]),np.array(obj_names2[i]),axis=0)
            obj_names.append(frame_names)
        else:
            frame_boxes =  np.array(boxes2[i]).astype(float)
            boxes.append(frame_boxes)
            frame_names = np.array(obj_names2[i])
            obj_names.append(frame_names)
        print(boxes[i].shape,obj_names[i].shape)
    res = []
    for i in range(len(imgs_names)):
        pick = non_max_suppression(np.array(boxes[i]).astype(float).astype(int), 0.8)
        selected_boxes,selected_names=np.array(boxes[i])[pick], np.array(obj_names[i])[pick]
        save_boxes_yolo(selected_boxes,selected_names,os.path.join("./","yolo_labels_relative.txt"),dict_,os.path.join(save_dir,imgs_names[i]))
