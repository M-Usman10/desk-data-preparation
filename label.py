import os
import pandas as pd
from Tools.preprocessing.readers import read_images
import cv2
img_dir="subset3"
labels_path="file.csv"
labels=[]
if os.path.exists(labels_path):
    labels=list(pd.read_csv(labels_path,header=None).values[:,1])
    print("Loaded file with {} labels".format(len(labels)))
imgs,names=read_images(img_dir,format=".jpg")
idx=len(labels)
while idx<len(imgs):
    img=imgs[idx]
    img=cv2.resize(img, (540, 540))
    cv2.imshow("img",img)
    key=chr(cv2.waitKey(0))
    if key=='a':
        labels.append(0)
        idx+=1
    elif key=='n':
        labels.append(1)
        idx+=1
    elif key=='u':
        labels.append(2)
        idx += 1
    elif key=='q':
        break
    else:
        print(key)
    cv2.destroyAllWindows()
    pd.Series(labels).to_csv(labels_path)

cv2.destroyAllWindows()
pd.Series(labels).to_csv(labels_path,)
print("\nLabelled {} out of {}".format(len(labels),len(imgs)))