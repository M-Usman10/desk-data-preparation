import os
import pandas as pd
from Tools.preprocessing.readers import read_images
from Tools.preprocessing.resizers import resize_image
import cv2
import numpy as np
def preprocess(img):
    img=resize_image(img,224,224)[0]
    return (img/127.5)-1

all_labels=[]
all_names=[]
all_images=[]
for i in range(1,4):
    img_dir="subset{}".format(i)
    labels_path = "file{}.csv".format(i)
    imgs,names=read_images(img_dir,format=".jpg",preprocess=preprocess)
    labels = pd.read_csv(labels_path, header=None).values[:, 1]
    imgs,names=imgs[:len(labels)],names[:len(labels)]
    selected=(labels!=2)
    all_labels.extend(labels[selected])
    all_names.extend(names[selected])
    all_images.extend(imgs[selected])

df = pd.DataFrame(np.array([all_names[:len(all_labels)],all_labels]).T,columns=["names","labels"])
df.to_csv("Data/labels/labels.csv".format(i))
np.save("labels.npy",all_labels)
np.save("images.npy",all_images)